from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserRegisterForm, UserUpdateForm, ProfilUpdateForm
from django.contrib.auth.models import User, Group
from novosti.models import Clanak
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Profil, Subscriber
from kviz.models import Rezultat
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
	ListView,
	DeleteView,
	CreateView,
)
class TeamListView(ListView):
	model = Profil
	template_name = 'korisnici/team_list.html'
	paginate_by = 5

	def get_context_data(self, **kwargs):
		context = super(TeamListView, self).get_context_data(**kwargs)

		#pagination mora bit ovako jer inace je overridano paginate_by iz super klase
		p = Paginator(Profil.objects.all(), self.paginate_by)

		context['activate'] = 'team'
		context['team'] = p.page(context['page_obj'].number)
		return context

class UserDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
	model = User
	success_url = '/korisnici/team/'

	def test_func(self):
		usr = self.request.user
		if usr.is_superuser:
			return True
		return False

# Create your views here.
def registracija(request):
	if request.method == 'POST':
		form = UserRegisterForm(request.POST)

		if form.is_valid():
			korisnik = form.save()
			korisnik.refresh_from_db() #load profile instance
			korisnik.profil.ime = form.cleaned_data.get('ime')
			korisnik.profil.prezime = form.cleaned_data.get('prezime')
			korisnik.profil.zanimanje = form.cleaned_data.get('zanimanje')
			korisnik.profil.broj = form.cleaned_data.get('broj')
			korisnik.save()
			#username = form.cleaned_data.get('username')

			messages.success(request, f'Kreiran korisnicki racun!')
			return redirect('login')
	else:
		form = UserRegisterForm()

	return render(request, 'korisnici/registracija.html', {'form': form, 'activate': 'registracija'})


@login_required
def profil_uredi(request):
	if request.method == 'POST':
		u_form = UserUpdateForm(request.POST, instance=request.user)
		p_form = ProfilUpdateForm(request.POST, request.FILES, instance=request.user.profil)

		if u_form.is_valid() and p_form.is_valid():
			u_form.save()
			p_form.save()

			messages.success(request, f'Promjene spremljene!')
			current_user = request.user
			return redirect('profil', username=current_user.username)
	else:
		u_form = UserUpdateForm(instance=request.user)
		p_form = ProfilUpdateForm(instance=request.user.profil)

	context = {
		'u_form': u_form,
		'p_form': p_form,
		'activate': 'profil'
	}

	return render(request, 'korisnici/profil-uredi.html', context)


#@login_required
def profil(request, username):
	korisnik = User.objects.get(username=username)
	clanak = Clanak.objects.filter(autor=korisnik).order_by('-datum_objave')
	rezultati = Rezultat.objects.filter(user=korisnik).order_by('-datum_rjesavanja')

	if request.POST.get('editor') == 'add':
		editors = Group.objects.get(name='Editors')
		editors.user_set.add(korisnik)
	elif request.POST.get('editor') == 'remove':
		editors = Group.objects.get(name='Editors')
		editors.user_set.remove(korisnik)

	query = request.GET.get('kviz')

	page = request.GET.get('page', 1)

	paginator = Paginator(clanak, 5)
	paginator1 = Paginator(rezultati, 5)

	try:
		clanci = paginator.page(page)
		rezultati = paginator1.page(page);
	except PageNotAnInteger:
		clanci = paginator.page(1)
		rezultati = paginator1.page(1);
	except EmptyPage:
		clanci = paginator.page(paginator.num_pages)
		rezultati = paginator1.page(paginator1.num_pages);

	if request.user == korisnik:
		activate = 'profil'
	else:
		activate = ''
	context = {
		'activate': activate,
		'object': korisnik,
		'posts': clanci,
		'rezultati': rezultati,
		'kviz': query
	}
	return render(request, 'korisnici/profil.html', context)


class SubscriberCreateView(CreateView):
	model = Subscriber
	fields = ['email',]
	template_name = 'korisnici/subscribe.html'
	success_url = '/'

	def form_valid(self, form):
		email = form.cleaned_data['email']
		if Subscriber.objects.filter(email=email).exists():
			messages.warning(self.request, f'Email vec prima obavijesti')
			return redirect('subscribe')
		return super().form_valid(form)
