from django.db.models.signals import post_save #fired after an object is saved
from django.contrib.auth.models import User
from django.dispatch import receiver
from .models import Profil

#kada je korisnik spremljen, instanciraj user-a
@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
	if created:
		Profil.objects.create(korisnik=instance)


@receiver(post_save, sender=User)
def save_profile(sender, instance, **kwargs):
	instance.profil.save() #ovo je mozda profile, nisam sigurna
