from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profil, Subscriber
from django.db.models import Q

class UserRegisterForm(UserCreationForm):
	email = forms.EmailField()
	ime = forms.CharField()
	prezime = forms.CharField()
	zanimanje = forms.CharField(required=False)
	broj = forms.CharField(required=False) #zasad ovako, kasnije prosiri kreirajuci PhoneField klasu
	#zivotopis = forms.FileField() #zasad jos ne al trebat ce vj

	class Meta:
		model = User
		fields = ['username', 'email', 'ime', 'prezime', 'zanimanje', 'broj', 'password1', 'password2']

	def clean(self, **kwargs):
		super(UserRegisterForm, self).clean(**kwargs);
		email  =  self.cleaned_data.get('email')
		if User.objects.filter(email = email).exists():
			raise forms.ValidationError("Email vec postoji")
		return self.cleaned_data


class UserUpdateForm(forms.ModelForm):
	email = forms.EmailField()

	class Meta:
		model = User
		fields = ['username', 'email']

	def clean(self, **kwargs):
		super(UserUpdateForm, self).clean(**kwargs)
		email  =  self.cleaned_data.get('email')
		if User.objects.filter(~Q(id = self.instance.id), email = email).exists():
			raise forms.ValidationError("Email vec postoji")
		return self.cleaned_data


class ProfilUpdateForm(forms.ModelForm):
	ime = forms.CharField()
	prezime = forms.CharField()
	zanimanje = forms.CharField(required=False)
	broj = forms.CharField(required=False)

	class Meta:
		model = Profil
		fields = ['ime', 'prezime', 'zanimanje', 'broj', 'slika']


#class SubscriberCreationForm(forms.ModelForm):
#	email = forms.EmailField()

#	class Meta:
#		model = Subscriber
#		fields = ['email',]
