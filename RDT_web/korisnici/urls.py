from django.contrib.auth import views as auth_views
from django.urls import path
from korisnici import views as korisnik_views
from django.conf.urls.static import static
from .views import (
    TeamListView,
    UserDeleteView,
    SubscriberCreateView,
)

urlpatterns = [
    path('team/', TeamListView.as_view(), name='team_list'),
    path('profil/<int:pk>/delete/', UserDeleteView.as_view(template_name='korisnici/user_confirm_delete.html'), name='user-delete'),
    path('registracija/', korisnik_views.registracija, name='registracija'),
    path('profil/<slug:username>/', korisnik_views.profil, name='profil'),
    path('profil_uredi/', korisnik_views.profil_uredi, name='profil-uredi'),
    path('login/', auth_views.LoginView.as_view(template_name='korisnici/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='korisnici/logout.html'), name='logout'),
    path('lozinka-reset/', auth_views.PasswordResetView.as_view(template_name='korisnici/lozinka_reset.html'), name='password_reset'),
    path('lozinka-reset/gotovo/', auth_views.PasswordResetDoneView.as_view(template_name='korisnici/lozinka_reset_done.html'), name='password_reset_done'),
    path('lozinka-reset-potvrdi/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='korisnici/lozinka_reset_confirm.html'), name='password_reset_confirm'),
    path('lozinka-reset-odradeno/', auth_views.PasswordResetCompleteView.as_view(template_name='korisnici/lozinka_reset_complete.html'), name='password_reset_complete'),
    path('subscribe/', SubscriberCreateView.as_view(), name='subscribe'),
]