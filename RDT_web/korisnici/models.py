from django.db import models
from django.contrib.auth.models import User
from PIL import Image

# Create your models here.
class Profil(models.Model):
	korisnik = models.OneToOneField(User, on_delete=models.CASCADE)
	ime = models.CharField(max_length=100)
	prezime = models.CharField(max_length=100)
	zanimanje = models.CharField(max_length=200, blank=True)
	broj = models.CharField(max_length=20, blank=True)
	slika = models.ImageField(default='default.jpg', upload_to='profile_pics')

	def __str__(self):
		return f'{self.korisnik.username} Profil'

	def save(self, **kwargs):
		super().save()

		img = Image.open(self.slika.path)

		if img.height > 300 or img.width > 300:
			output_size = (300, 300)
			img.thumbnail(output_size)
			img.save(self.slika.path)


class Subscriber(models.Model):
	email = models.EmailField(max_length=100)

	def __str__(self):
		return f'{self.email}'
