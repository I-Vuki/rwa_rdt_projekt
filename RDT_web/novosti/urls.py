from django.urls import path
from . import views
from .views import (
	PostListView, 
	PostDetailView,
	PostCreateView,
	PostUpdateView,
	PostDeleteView,
    ImageDeleteView
	)

urlpatterns = [
    path('', PostListView.as_view(), name='novosti-home'),
    path('clanak/<int:pk>/', PostDetailView.as_view(), name='clanak-detail'),
    path('clanak/novi/', PostCreateView.as_view(), name='clanak-create'),
    path('clanak/<int:pk>/uredi/', PostUpdateView.as_view(), name='clanak-update'),
    path('clanak/<int:pk>/obrisi/', PostDeleteView.as_view(), name='clanak-delete'),
    path('info/', views.info, name='novosti-info'),
    path('pretraga/', views.search, name='pretraga'),
    path('clanak/<int:pk>/slike/', views.clanak_slike, name='clanak-slike'),
    path('slika/<int:pk>/obrisi/', ImageDeleteView.as_view(), name='slika-obrisi'),
]