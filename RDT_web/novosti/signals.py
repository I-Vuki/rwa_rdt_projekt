from django.db.models.signals import post_save #fired after an object is saved
from .models import Clanak
from django.dispatch import receiver
from django.core.mail import send_mail
#from django.contrib.auth.models import User
from korisnici.views import Subscriber
from RDT_web import settings
from django.template.loader import render_to_string


#@receiver(post_save, sender=Clanak)
def send_to_subs(sender, created, **kwargs):
	if created:
		try:
			for korisnik in Subscriber.objects.all():
				mailto = korisnik.email
				subject = "Novi clanak u RDT-u"
				content = "Novi clanak je objavljen na RDT stranici, pogledaj!"
				mailfrom = settings.EMAIL_HOST_USER
				html_message = render_to_string('novosti/mail_template.html')
				send_mail(subject, content, mailfrom, [mailto], html_message=html_message)
		except: pass
post_save.connect(send_to_subs, sender=Clanak)