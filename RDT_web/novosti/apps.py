from django.apps import AppConfig


class NovostiConfig(AppConfig):
    name = 'novosti'

    def ready(self):
    	import novosti.signals
