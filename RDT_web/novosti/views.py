from django.shortcuts import render, redirect, reverse
from .models import Clanak
from django.views.generic import (
	ListView,
	DetailView,
	CreateView,
	UpdateView,
	DeleteView
	)
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.paginator import Paginator
from django.contrib.auth.models import User
from galerija.models import Image
from django.contrib.auth.decorators import login_required
from galerija.forms import PostImageUploadForm
from django.contrib import messages

# Create your views here.

#ovo je class based view za novosti.html
class PostListView(ListView):
	model = Clanak
	paginate_by = 5
	template_name = 'novosti/novosti.html'

	#ordering = ['-datum_objave'] #ovo iz nekog razloga ne radi kad definiram get_context_data
	#context_object_name = 'posts'

	def get_context_data(self, **kwargs):
		context = super(PostListView, self).get_context_data(**kwargs)

		#pagination mora bit ovako jer inace je overridano paginate_by iz super klase
		p = Paginator(Clanak.objects.all().order_by('-datum_objave'), self.paginate_by)

		context['activate'] = 'novosti'
		context['posts'] = p.page(context['page_obj'].number)
		return context


class PostDetailView(DetailView):
	model = Clanak
	#po defaultu trazi template u obliku: <app>/<model>_<viewtype>.html

	def get_context_data(self, **kwargs):
		context = super(PostDetailView, self).get_context_data(**kwargs)

		context['images'] = Image.objects.filter(clanak=self.kwargs.get('pk'))[:20] #p.page(context['page_obj'].number)
		return context


class PostCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
	model = Clanak
	fields = ['naslov', 'sadrzaj']
	#po defaultu trazi template u obliku: <app>/<model>_<viewtype>.html (za createview viewtype je form)

	def form_valid(self, form):
		form.instance.autor = self.request.user
		return super().form_valid(form)

	def get_context_data(self, **kwargs):
		context = super(PostCreateView, self).get_context_data(**kwargs)
		context['activate'] = 'novi_clanak'
		return context

	def test_func(self):
		usr = self.request.user
		if usr.groups.filter(name='Editors').exists():
			return True
		return False


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
	model = Clanak
	fields = ['naslov', 'sadrzaj']
	#po defaultu trazi template u obliku: <app>/<model>_<viewtype>.html (za updateview viewtype je form)

	def get_context_data(self, **kwargs):
		context = super(PostUpdateView, self).get_context_data(**kwargs)
		new_image_form = PostImageUploadForm()
		context['activate'] = 'novi_clanak'
		return context

	def form_valid(self, form):
		form.instance.autor = self.request.user
		return super().form_valid(form)

	def test_func(self):
		clanak = self.get_object()
		usr = self.request.user
		if usr == clanak.autor and usr.groups.filter(name='Editors').exists():
			return True
		return False


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
	model = Clanak
	success_url = '/novosti/'
	#po defaultu trazi template u obliku: <app>/<model>_<viewtype>.html (ovdje je clanak_confirm_delete.html)

	def test_func(self):
		clanak = self.get_object()
		usr = self.request.user
		if usr == clanak.autor and usr.groups.filter(name='Editors').exists():
			return True
		return False



def info(request):
	return render(request, 'novosti/info.html', {'activate': 'info'})


def search(request):
	val = request.GET.get('value')
	usr = User.objects.filter(username__contains=val)
	clanak = Clanak.objects.filter(naslov__contains=val).order_by('-datum_objave')
	clanak |= Clanak.objects.filter(autor__in=usr).order_by('-datum_objave')
	clanak |= Clanak.objects.filter(sadrzaj__contains=val).order_by('-datum_objave')
	page = request.GET.get('page', 1)

	paginator = Paginator(clanak, 5)
	try:
		clanci = paginator.page(page)
	except PageNotAnInteger:
		clanci = paginator.page(1)
	except EmptyPage:
		clanci = paginator.page(paginator.num_pages)

	context = {
		'posts': clanci
	}
	return render(request, 'novosti/search.html', context)


@login_required
def clanak_slike(request, pk):
	usr = request.user
	cl = Clanak.objects.filter(pk=pk).first()
	slike = Image.objects.filter(clanak=cl)

	if usr != cl.autor:
		return redirect('clanak-detail', pk)

	if usr.groups.filter(name='Editors').exists():
		if request.method == 'POST':
			for _file in request.FILES.getlist('image'):
				list = {'image' :_file, }
				form = PostImageUploadForm(request.POST, list)
				if(not _file.name.lower().endswith(('.avi', '.mp4', '.png', '.jpg', 'jpeg'))):
					messages.warning(request, f'Nedozvoljeni format')
					form = PostImageUploadForm()
					break
				if form.is_valid():
					new = form.save(commit=False)
					new.image = form.cleaned_data['image']
					new.clanak = cl
					if(_file.name.lower().endswith(('.avi', 'mp4'))):
						new.slika = False
					new.save()
					form.save_m2m()
					messages.success(request, f'Slika uspjesno dodana')
				else:
					messages.error(request, "Incorrect file type")
			return redirect('clanak-detail', pk)
		else:
			form = PostImageUploadForm()

		context = {
			'slike': slike,
			'form': form,
			'activate': 'image_upload',
		}
		return render(request, 'novosti/slike.html', context)
	else:
		return redirect('login')


class ImageDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
	model = Image
	#success_url = '' #promjeni
	#po defaultu trazi template u obliku: <app>/<model>_<viewtype>.html (ovdje je clanak_confirm_delete.html)

	def test_func(self):
		slika = self.get_object()
		clanak = slika.clanak
		usr = self.request.user
		if usr == clanak.autor and usr.groups.filter(name='Editors').exists():
			return True
		return False

	def get_success_url(self, **kwargs):
		return reverse('clanak-slike', kwargs={'pk': self.object.clanak_id})
