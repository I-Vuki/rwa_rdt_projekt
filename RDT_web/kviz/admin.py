from django.contrib import admin

# Register your models here.
from .models import Pitanja, Odgovori, Rezultat

admin.site.register(Pitanja)
admin.site.register(Odgovori)
admin.site.register(Rezultat)
