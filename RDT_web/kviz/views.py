from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import Pitanja, Odgovori, Rezultat
from django.contrib.auth.models import User
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Pitanja, Odgovori
from django.views.generic import (
	ListView,
	DetailView,
	CreateView,
	UpdateView,
	DeleteView
	)

@login_required
def home(request):
    br_pitanja = 20
    br_pitanja_limit = Pitanja.objects.count()
    if br_pitanja >= br_pitanja_limit:
        br_pitanja = br_pitanja_limit
    sva_pitanja = Pitanja.objects.order_by('?')[:br_pitanja]
    context = {
        'lista_pitanja': sva_pitanja,
        'activation': 'kviz'
    }
    return render(request, 'kviz/home.html', context)


@login_required
def rezultati(request):
    score = 0
    brpit = 0
    if request.method == 'POST':
        pitanja = Pitanja.objects.all()

        for p in pitanja:
            odgovor = Odgovori.objects.filter(pitanje=p, tocan=True)
            odgovor = odgovor.first()
            odgval = request.POST.get('odg' + str(p.id), 0)
            if odgval == str(odgovor.id):
                score += 1
            brpit += 1

    percent = score/brpit * 100
    noviRez = Rezultat(user=request.user,rezultat=percent)
    previous_page = request.META.get('HTTP_REFERER')
    if previous_page != request.build_absolute_uri('/kviz/'):
        return redirect('kviz:kviz_home')

    noviRez.save()
    context = {
        'score': score,
        'brpit': brpit
    }
    return render(request, 'kviz/rezultati.html', context)

class KvizListView(LoginRequiredMixin, UserPassesTestMixin, ListView):
	model = Pitanja
	template_name = 'kviz/pitanja_lista.html'
	paginate_by = 13

	def get_context_data(self, **kwargs):
		context = super(KvizListView, self).get_context_data(**kwargs)
		pitanja = Pitanja.objects.all()

		page = self.request.GET.get('page', 1)

		#pagination mora bit ovako jer inace je overridano paginate_by iz super klase
		paginator_pit = Paginator(pitanja, self.paginate_by)

		try:
			context['pitanja'] = paginator_pit.page(page)
		except PageNotAnInteger:
			context['pitanja'] = paginator_pit.page(1)
		except EmptyPage:
			context['pitanja'] = paginator_pit.page(paginator_pit.num_pages)

		context['activate'] = 'kviz_lista'
		return context

	def test_func(self):
		usr = self.request.user
		if usr.groups.filter(name='Editors').exists():
			return True
		return False


class KvizDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
	model = Pitanja


	def get_context_data(self, **kwargs):
		context = super(KvizDetailView, self).get_context_data(**kwargs)
		context['activate'] = 'kviz_lista'
		return context

	def test_func(self):
		usr = self.request.user
		if usr.groups.filter(name='Editors').exists():
			return True
		return False


class KvizCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
	model = Pitanja
	fields = ['text_pitanja']

	def get_context_data(self, **kwargs):
		context = super(KvizCreateView, self).get_context_data(**kwargs)
		context['activate'] = 'kviz_lista'
		return context

	def test_func(self):
		usr = self.request.user
		if usr.groups.filter(name='Editors').exists():
			return True
		return False


class KvizUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
	model = Pitanja
	fields = ['text_pitanja']

	def get_context_data(self, **kwargs):
		context = super(KvizUpdateView, self).get_context_data(**kwargs)
		context['activate'] = 'kviz_lista'
		return context

	def test_func(self):
		usr = self.request.user
		if usr.groups.filter(name='Editors').exists():
			return True
		return False


class KvizDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
	model = Pitanja
	success_url = '/kviz/pitanja_lista/'

	def get_context_data(self, **kwargs):
		context = super(KvizDeleteView, self).get_context_data(**kwargs)
		context['activate'] = 'kviz_lista'
		return context


	def test_func(self):
		usr = self.request.user
		if usr.groups.filter(name='Editors').exists():
			return True
		return False

class OdgovorDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
	model = Odgovori

	def get_context_data(self, **kwargs):
		context = super(OdgovorDetailView, self).get_context_data(**kwargs)
		context['activate'] = 'kviz_lista'
		return context

	def test_func(self):
		usr = self.request.user
		if usr.groups.filter(name='Editors').exists():
			return True
		return False


class OdgovorCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
	model = Odgovori
	fields = ['text_odgovora', 'tocan', 'pitanje']

	def get_context_data(self, **kwargs):
		context = super(OdgovorCreateView, self).get_context_data(**kwargs)
		context['activate'] = 'kviz_lista'
		return context

	def test_func(self):
		usr = self.request.user
		if usr.groups.filter(name='Editors').exists():
			return True
		return False


class OdgovorUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
	model = Odgovori
	fields = ['text_odgovora', 'tocan', 'pitanje']

	def get_context_data(self, **kwargs):
		context = super(OdgovorUpdateView, self).get_context_data(**kwargs)
		context['activate'] = 'kviz_lista'
		return context

	def test_func(self):
		usr = self.request.user
		if usr.groups.filter(name='Editors').exists():
			return True
		return False


class OdgovorDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
	model = Odgovori
	success_url = '/kviz/odgovori_lista/'

	def get_context_data(self, **kwargs):
		context = super(OdgovorDeleteView, self).get_context_data(**kwargs)
		context['activate'] = 'kviz_lista'
		return context

	def test_func(self):
		usr = self.request.user
		if usr.groups.filter(name='Editors').exists():
			return True
		return False

class OdgovorListView(LoginRequiredMixin, UserPassesTestMixin, ListView):
	model = Odgovori
	template_name = 'kviz/odgovori_lista.html'
	paginate_by = 13

	def get_context_data(self, **kwargs):
		context = super(OdgovorListView, self).get_context_data(**kwargs)
		odgovori = Odgovori.objects.all()

		page = self.request.GET.get('page', 1)

		#pagination mora bit ovako jer inace je overridano paginate_by iz super klase
		paginator_odg = Paginator(odgovori, self.paginate_by)

		try:
			context['odgovori'] = paginator_odg.page(page);
		except PageNotAnInteger:
			context['odgovori'] = paginator_odg.page(1);
		except EmptyPage:
			context['odgovori'] = paginator_odg.page(paginator_odg.num_pages);

		context['activate'] = 'kviz_lista'
		return context

	def test_func(self):
		usr = self.request.user
		if usr.groups.filter(name='Editors').exists():
			return True
		return False
