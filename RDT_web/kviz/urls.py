from django.urls import path
from .views import home, rezultati
from .views import (
	KvizListView,
	KvizDetailView,
	KvizCreateView,
	KvizUpdateView,
	KvizDeleteView,
	OdgovorListView,
	OdgovorDetailView,
	OdgovorCreateView,
	OdgovorUpdateView,
	OdgovorDeleteView,
)

app_name='kviz'

urlpatterns = [
    path('', home, name='kviz_home'),
    path('rezultati/', rezultati, name='rezultati'),
    path('pitanja_lista/',KvizListView.as_view(), name='pitanja_lista'),
	path('pitanje_detalji/<int:pk>/', KvizDetailView.as_view(), name='pitanje_detalji'),
	path('pitanje_update/<int:pk>/uredi/',KvizUpdateView.as_view(), name='pitanje_update'),
	path('pitanje_delete/<int:pk>/obrisi/', KvizDeleteView.as_view(), name='pitanje_delete'),
    path('pitanje_add/', KvizCreateView.as_view(), name='pitanje_add'),
    path('odgovori_lista/',OdgovorListView.as_view(), name='odgovori_lista'),
	path('odgovor_detalji/<int:pk>/', OdgovorDetailView.as_view(), name='odgovor_detalji'),
	path('odgovor_update/<int:pk>/uredi/',OdgovorUpdateView.as_view(), name='odgovor_update'),
	path('odgovor_delete/<int:pk>/obrisi/', OdgovorDeleteView.as_view(), name='odgovor_delete'),
    path('odgovor_add/', OdgovorCreateView.as_view(), name='odgovor_add'),
]
