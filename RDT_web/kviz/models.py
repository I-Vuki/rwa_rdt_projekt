from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils import timezone
from django.urls import reverse

# Create your models here.
class Pitanja(models.Model):
    text_pitanja = models.TextField()

    def __str__(self):
        return self.text_pitanja

    def get_absolute_url(self):
        return reverse('kviz:pitanja_lista')


class Odgovori(models.Model):
    text_odgovora = models.TextField()
    tocan = models.BooleanField(default=0)
    pitanje = models.ForeignKey(Pitanja, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.text_odgovora}'

    def get_absolute_url(self):
        return reverse('kviz:odgovori_lista')

    def clean(self):
    	if self.tocan and Odgovori.objects.filter(pitanje=self.pitanje).filter(tocan=self.tocan).exists():
            raise ValidationError('Vec postoji tocan odgovor na to pitanje')


class Rezultat(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    datum_rjesavanja = models.DateTimeField(default=timezone.now)
    rezultat = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return f'{self.user} - rezultat: {self.datum_rjesavanja}'
