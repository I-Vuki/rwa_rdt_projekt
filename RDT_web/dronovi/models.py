from django.db import models
from PIL import Image
from django.urls import reverse
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from calendar import HTMLCalendar
from datetime import date
from itertools import groupby
import datetime

# Create your models here.
class Dron(models.Model):
	naziv = models.CharField(max_length=200)
	registracija = models.CharField(max_length=100)
	opis = models.TextField(blank=True)
	slika = models.ImageField(default='dron_default.jpg', upload_to='dron_pics')

	def __str__(self):
		return f'{self.naziv}'

	def save(self, **kwargs):
		super().save()

		img = Image.open(self.slika.path)

		if img.height > 300 or img.width > 300:
			output_size = (300, 300)
			img.thumbnail(output_size)
			img.save(self.slika.path)

	def get_absolute_url(self):
		return reverse('dron-detail', kwargs={'pk': self.pk})

class Let(models.Model):
	datum = models.DateField(u'Datum', help_text=u'Datum leta YYYY-MM-DD')
	start_time = models.TimeField(u'Vrijeme pocetka', help_text=u'Vrijeme pocetka hh:mm')
	end_time = models.TimeField(u'Vrijeme zavrsetka', help_text=u'Vrijeme zarsetka hh:mm')
	note = models.TextField(u'Note', help_text=u'Napomene za let', blank=True, null=True)
	#stani kljucevi idkorisnik iddron
	#promjeniti on delete brisat samo buduce letove
	korisnik = models.ForeignKey(User, on_delete = models.CASCADE)
	dron = models.ForeignKey(Dron, on_delete = models.CASCADE)

	def __str__(self):
		return f'{self.korisnik} - {self.dron}'

	def check_overlap(self, fixed_start, fixed_end, new_start, new_end):
		overlap = False
		if new_start == fixed_end or new_end == fixed_start:    #edge case
	   		overlap = False
		elif (new_start >= fixed_start and new_start <= fixed_end) or (new_end >= fixed_start and new_end <= fixed_end): #innner limits
			overlap = True
		elif new_start <= fixed_start and new_end >= fixed_end: #outter limits
			overlap = True

		return overlap

	def get_absolute_url(self):
	   	url = reverse('admin:%s_%s_change' % (self._meta.app_label, self._meta.model_name), args=[self.id])
	   	return u'<a href="%s">%s</a>' % (url, str(self.start_time))

	def clean(self):
		if not self.end_time or not self.start_time:
	   		raise ValidationError('Vrijeme pocetka i zavrsetka mora biti moguce')

		if self.end_time <= self.start_time:
	   		raise ValidationError('Vrijeme pocetka mora biti prije vremena zavrsetka')

		RDT_start = datetime.date(2015, 1, 1)
		if self.datum < RDT_start:
			raise ValidationError('RDT nije postojao tada, odaberite drugi datum')
		now = datetime.datetime.now()
		year_now = now.year
		month_now = now.month
		day_now = now.day
		buducnost = datetime.date(year_now+1, month_now, day_now)
		if self.datum > buducnost:
			raise ValidationError('Ne moze se rezervirati dron za toliko unarijed')

		rezervacije = Let.objects.filter(datum=self.datum).filter(dron=self.dron)
		if rezervacije.exists():
			for rezervacija in rezervacije:
				if self.check_overlap(rezervacija.start_time, rezervacija.end_time, self.start_time, self.end_time):
					raise ValidationError('Drone je rezerviran: ' + str(rezervacija.datum) + ', ' + str(rezervacija.start_time) + '-' + str(rezervacija.end_time))

class RezervacijaKalendar(HTMLCalendar):
	def __init__(self):
		super(RezervacijaKalendar, self).__init__()

	def formatday(self, day):
		if day != datetime.date(1800,1,1):
			letovi_danas = self.get_drones(day)
			let_html = '<small class="text-right text-muted">'+str(day.day)+'</small>'
			if len(letovi_danas) > 3:
				for let in letovi_danas[0:2]:
					let_html += '<div class="dropdown">'
					let_html += '<button type="button" class="btn-sm btn-light text-primary text-secondery dropdown-toggle" data-toggle="dropdown">' + str(let["dron"]) + '</button>'
					let_html += '<div class="dropdown-menu">'
					let_html += '<span class="dropdown-item">'
					let_html += str(let["start"]) + "-" + str(let["end"])
					let_html += '</span></div></div>'

				let_html += '<div class="dropdown">'
				let_html += '<button type="button" class="btn-sm btn-light text-primary text-secondery dropdown-toggle" data-toggle="dropdown"> . . .</button>'
				let_html += '<div class="dropdown-menu">'
				for let in letovi_danas[2:]:
					let_html += '<span class="dropdown-item">'
					let_html += str(let["dron"]) + ": " +str(let["start"]) + "-" + str(let["end"])
					let_html += '</span>'
				let_html += '</div></div>'
			else:
				for let in letovi_danas:
					let_html += '<div class="dropdown">'
					let_html += '<button type="button" class="btn-sm btn-light text-primary text-secondery dropdown-toggle" data-toggle="dropdown">' + str(let["dron"]) + '</button>'
					let_html += '<div class="dropdown-menu">'
					let_html += '<span class="dropdown-item">'
					let_html += str(let["start"]) + "-" + str(let["end"])
					let_html += '</span></div></div>'
		else:
			let_html = ''

		return '<td  width="120" height="150">' + let_html + '</div>'

	def formatweek(self, week):
		week_html = '<tr>'
		for day in week:
			week_html += self.formatday(day)
		week_html += '</tr>'
		return week_html

	def formatmonth(self, god, mj, wyear=True):
		month = []
		month.append('<table class="table table-sm table-responsive">')
		month.append(self.formatmonthname(mj, god))
		month.append(self.foratweekheader())
		mj2 = mj + 1
		god2 = god
		if mj2 > 12:
			god2 += 1
			mj2 = 1
		start_date = datetime.date(god, mj, 1)
		end_date = datetime.date(god2, mj2, 2)
		week = []
		for day in self.daterange(start_date,end_date):
			week.append(day)
			if day.weekday() == 6 and day != datetime.date(god2, mj2, 1):
				rng = 7 - len(week)
				for i in range(rng):
					zero = datetime.date(1800,1,1)
					week.insert(0,zero)
				month.append(self.formatweek(week))
				week = []
			if day == datetime.date(god2, mj2, 1):
				if week:
					week.pop()
				rng = 7 - len(week)
				for i in range(rng):
					zero = datetime.date(1800,1,1)
					week.append(zero)
				month.append(self.formatweek(week))
				week = []
		month.append('</table>')
		return ''.join(month)

	def foratweekheader(self):
		dani = ['PON', 'UTO', 'SRI', 'CET','PET', 'SUB', 'NED']
		s = '<tr style="text-align:center">'
		for dan in dani:
			s += '<th>' + dan + '</th>'
		s += '</tr>'
		return s

	def formatmonthname(self, month, year):
		mjeseci = [ '', 'Siječanj', 'Veljača', 'Ožujak' , 'Travanj', 'Svibanj','Lipanj', 'Srpanj', 'Kolovoz', 'Rujan', 'Listopad','Studeni', 'Prosinac ']
		s = '<tr><th colspan=7 style="text-align: center;">'
		s += mjeseci[month] + " "
		s += str(month) + "/"
		s += str(year)
		s += '</th></tr>'
		return s

	def daterange(self, start_date, end_date):
	    for n in range(int ((end_date - start_date).days)):
	        yield start_date + datetime.timedelta(n)

	def get_drones(self, danas):
		lista_letova = []
		letovi_danas = Let.objects.filter(datum=danas)
		if letovi_danas.exists():
			for let in letovi_danas:
				dic = {}
				dic['dron'] = let.dron
				dic['start'] = let.start_time
				dic['end'] = let.end_time
				lista_letova.append(dic)
		return lista_letova
