from django.shortcuts import render, redirect, render_to_response
from .forms import FlightRegForm
from .models import Dron, Let, RezervacijaKalendar
from galerija.models import Image
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.paginator import Paginator
from django.contrib import messages
from django.utils.safestring import SafeString, mark_safe
# Create your views here.
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
import datetime
from django.views.generic import (
	ListView,
	DetailView,
	CreateView,
	UpdateView,
	DeleteView
	)

class DronListView(ListView):
	model = Dron
	template_name = 'dronovi/dronovi.html'
	paginate_by = 5

	def get_context_data(self, **kwargs):
		context = super(DronListView, self).get_context_data(**kwargs)

		#pagination mora bit ovako jer inace je overridano paginate_by iz super klase
		p = Paginator(Dron.objects.all(), self.paginate_by)

		context['activate'] = 'dronovi'
		context['drones'] = p.page(context['page_obj'].number)
		return context


class DronDetailView(DetailView):
	model = Dron
	#paginate_by = 50

	def get_context_data(self, **kwargs):
		context = super(DronDetailView, self).get_context_data(**kwargs)

		#pagination mora bit ovako jer inace je overridano paginate_by iz super klase
		#p = Paginator(Image.objects.all(), self.paginate_by)

		context['images'] = Image.objects.filter(dron=self.kwargs.get('pk')) #p.page(context['page_obj'].number)
		context['activate'] = 'novi_dron'
		return context



class DronCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
	model = Dron
	fields = ['naziv', 'registracija', 'opis', 'slika']

	def get_context_data(self, **kwargs):
		context = super(DronCreateView, self).get_context_data(**kwargs)
		context['activate'] = 'novi_dron'
		return context

	def test_func(self):
		usr = self.request.user
		if usr.groups.filter(name='Editors').exists():
			return True
		return False


class DronUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
	model = Dron
	fields = ['naziv', 'registracija', 'opis', 'slika']

	def test_func(self):
		usr = self.request.user
		if usr.groups.filter(name='Editors').exists():
			return True
		return False


class DronDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
	model = Dron
	success_url = '/dronovi/'

	def test_func(self):
		usr = self.request.user
		if usr.groups.filter(name='Editors').exists():
			return True
		return False

@login_required
def rezervacija_leta(request):
	if request.method == 'POST':
		form = FlightRegForm(request.POST)

		if form.is_valid():
			let = form.save(commit=False)
			let.korisnik = request.user
			#let.dron = form.cleaned_data('dron')
			#let.datum = form.cleaned_data('datum')
			#let.start_time = form.cleaned_data('start_time')
			#let.end_time = form.cleaned_data('end_time')
			let.save()

			messages.success(request, f'Uspjesno ste rezervirali let!')
			return redirect('kalendar_leta_default')
	else:
		form = FlightRegForm()

	return render(request, 'dronovi/rezervacija_leta.html', {'form': form, 'activate': 'rezervacija_leta'})

@login_required
def rezervacija_kalendar(request, god, mj):
	cal = RezervacijaKalendar().formatmonth(god, mj)
	context = {
		'kalendar': mark_safe(cal),
		'activate': 'kalendar',
		'god': god,
		'mj': mj,
	}
	return render(request, 'dronovi/kalendar.html', context)

def rezervacija_kalendar_def(request):
    danas = datetime.datetime.now()
    god = danas.year
    mj = danas.month
    put = request.path + str(god) + '/' + str(mj) +'/'
    return HttpResponseRedirect(put)
