from django.urls import path
from dronovi import views as dron_views
from .views import (
	DronListView,
	DronDetailView,
	DronCreateView,
	DronUpdateView,
	DronDeleteView
	)

urlpatterns = [
	path('', DronListView.as_view(), name='dronovi-home'),
	path('dron/<int:pk>/', DronDetailView.as_view(), name='dron-detail'),
	path('dron/<int:pk>/uredi/', DronUpdateView.as_view(), name='dron-update'),
	path('dron/<int:pk>/obrisi/', DronDeleteView.as_view(), name='dron-delete'),
	path('novi/', DronCreateView.as_view(), name='dron-create'),
	path('rezervacija_leta/', dron_views.rezervacija_leta, name='rezervacija_leta'),
	path('rezervacija_kalendar/', dron_views.rezervacija_kalendar_def, name='kalendar_leta_default'),
	path('rezervacija_kalendar/<int:god>/<int:mj>/',dron_views.rezervacija_kalendar, name='kalendar_leta'),
]
