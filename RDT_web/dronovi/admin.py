from django.contrib import admin
from .models import Dron, Let

# Register your models here.
admin.site.register(Dron)
admin.site.register(Let)
