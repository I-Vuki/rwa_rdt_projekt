from django import forms
from .models import Let

class FlightRegForm(forms.ModelForm):
    class Meta:
        model = Let
        fields = ['dron', 'datum', 'start_time', 'end_time', 'note']
