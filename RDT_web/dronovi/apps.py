from django.apps import AppConfig


class DronoviConfig(AppConfig):
    name = 'dronovi'
