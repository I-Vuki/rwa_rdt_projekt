"""RDT_web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from korisnici import views as korisnik_views
from django.conf import settings
from django.conf.urls.static import static
from galerija import views as galerija_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('novosti/', include('novosti.urls')),
    path('korisnici/', include('korisnici.urls')),
    path('dronovi/', include('dronovi.urls')),
    path('kviz/', include('kviz.urls')),
    path('galerija/', include('galerija.urls')),
    path('', galerija_views.carousel, name='carousel'),
]

#ovo je ovako samo zbog lakse citljivosti, mozes + static(...) dodat iza uglate zagrade u prethodnom redu
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
