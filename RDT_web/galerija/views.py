from django.shortcuts import render, reverse, redirect
from django.contrib import messages
from .models import Image
#from django.views.generic.edit import FormView
from .forms import ImageUploadForm
from django.contrib.auth.decorators import login_required
from django.views.generic import DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin


def galerija_slike(request):
	querysetimg = Image.objects.all()
	context = {
		"images": querysetimg,
		"activate": "galerija"
	}
	return render(request, 'galerija/galerija_slike.html', context)

def slike_pretraga(request):
	val = request.GET.get('value')
	val = val.lower();
	if(val == ""):
		slike = Image.objects.all()
	else:
		tagovi = val.split(" ")
		slike = Image.objects.filter(tags__name__in=[tagovi[0]]).distinct()
		for tag in tagovi:
			slike |= Image.objects.filter(tags__name__in=[tag]).distinct()

	context = {
		'images': slike
	}
	return render(request, 'galerija/galerija_slike.html', context)

def carousel(request):
	querysetimg = Image.objects.filter(slika=1)[:5]
	context = {
		"images": querysetimg,
	}
	return render(request, 'galerija/carousel.html', context)

#jedan tag za sve koje odjednom uploadas dodaj
#povezi s dronom, userom, clankom
@login_required
def image_upload(request):
	usr = request.user

	if usr.groups.filter(name='Editors').exists():
		if request.method == 'POST':
			for _file in request.FILES.getlist('image'):
				list = {'image' : _file, }
				form = ImageUploadForm(request.POST, list)
				if(not _file.name.lower().endswith(('.avi', '.mp4', '.png', '.jpg', '.jpeg'))):
					messages.warning(request, f'Nedozvoljeni format')
					form = ImageUploadForm()
					break

				if form.is_valid():
					new = form.save(commit=False)
					new.image = form.cleaned_data['image']
					new.autor = request.user
					if(_file.name.lower().endswith(('.avi', '.mp4'))):
						new.slika = False
					new.save()
					form.save_m2m()
					messages.success(request, f'Slika/Video uspjesno dodan')
				else:
					messages.error(request, "Incorrect file type")
			return redirect('galerija')
		else:
			form = ImageUploadForm()

		context = {
			'form': form,
			'activate': 'image_upload',
		}
		return render(request, 'galerija/slike_dodaj.html', context)
	else:
		return redirect('login')



class MediaEditView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
	model = Image
	fields = ['tags', 'dron', 'clanak',]
	template_name = 'galerija/slike_uredivanje.html'
	success_url = '/galerija/slike/'

	def get_context_data(self, **kwargs):
		context = super(MediaEditView, self).get_context_data(**kwargs)

		context['image'] = self.get_object()
		return context

	def test_func(self):
		usr = self.request.user
		if usr.groups.filter(name='Editors').exists():
			return True
		return False



class MediaDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
	model = Image
	success_url = '/galerija/slike/' #promjeni
	#po defaultu trazi template u obliku: <app>/<model>_<viewtype>.html (ovdje je clanak_confirm_delete.html)

	def test_func(self):
		slika = self.get_object()
		clanak = slika.clanak
		usr = self.request.user
		if usr.groups.filter(name='Editors').exists():
			return True
		return False

	#def get_success_url(self, **kwargs):
	#	return reverse('clanak-slike', kwargs={'pk': self.object.clanak_id})
