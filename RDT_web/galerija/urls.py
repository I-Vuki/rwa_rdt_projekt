from django.contrib.auth import views as auth_views
from django.urls import path
from galerija import views as galerija_views
from galerija.views import MediaDeleteView, MediaEditView
from django.conf.urls.static import static

urlpatterns = [
    path('slike/', galerija_views.galerija_slike, name='galerija'),
    path('slike/pretraga', galerija_views.slike_pretraga, name='slike-pretraga'),
    #path('video/', galerija_views.galerija_video, name='galerija-video'),
    path('upload/', galerija_views.image_upload, name='galerija-upload'),
    path('slika/<int:pk>/obrisi/', MediaDeleteView.as_view(), name='galerija-delete'),
    path('slika/<int:pk>/uredi/', MediaEditView.as_view(), name='galerija-edit'),
    path('carousel/', galerija_views.carousel, name='carousel'),
]