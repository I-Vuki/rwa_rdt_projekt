from django import forms
from .models import Image

class ImageUploadForm(forms.ModelForm):
	image = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))

	class Meta:
		model = Image
		fields = ['image', 'tags', 'dron', 'clanak',]

	def clean(self, **kwargs):
		return self.cleaned_data

class PostImageUploadForm(forms.ModelForm):
	image = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))

	class Meta:
		model = Image
		fields = ['image', 'tags', 'dron',]

	def clean(self, **kwargs):
		return self.cleaned_data
