from django.contrib.auth.models import User
from taggit.managers import TaggableManager
from dronovi.models import Dron
from novosti.models import Clanak
from django.utils import timezone
from django.db import models
from PIL import Image

class Image(models.Model):
	image = models.FileField(upload_to='gallery_pics')
	tags = TaggableManager()
	timestamp = models.DateTimeField(default=timezone.now)
	autor = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
	dron = models.ForeignKey(Dron, on_delete=models.SET_NULL, null=True, blank=True)
	clanak = models.ForeignKey(Clanak, on_delete=models.SET_NULL, null=True, blank=True)
	slika = models.BooleanField(default=True)

	def __str__(self):
		return f'{self.image}'

	class Meta:
		ordering = ["-timestamp"]
