# Generated by Django 2.1.5 on 2019-01-30 12:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('galerija', '0005_auto_20190130_1254'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='slika',
            field=models.BooleanField(default=True),
        ),
    ]
